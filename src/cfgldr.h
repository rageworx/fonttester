#ifndef __CFGLDR_H__
#define __CFGLDR_H__

#include <string>

class ConfigLoader
{
    public:
        ConfigLoader( const char* bd = NULL, const char* an = NULL, const char* fn = NULL );
        ConfigLoader( const wchar_t* bd = NULL, const wchar_t* an = NULL, const wchar_t* fn = NULL );
        ~ConfigLoader();

    public:
        bool GetWindowPos( int &x, int &y, int &w, int &h, int &d );
        bool SetWindowPos( int x, int y, int w, int h, int d = 0 );
        bool GetFontSize( unsigned &sz );
        bool SetFontSize( unsigned sz );
        bool GetUserText( char* &u8t );
        bool SetUserText( const char* u8t = NULL );
        bool GetLastPath( char* &path );
        bool GetLastPath( wchar_t* &path );
        bool SetLastPath( const char* path = NULL );
        bool SetLastPath( const wchar_t* path = NULL );

    protected:
        void init_a( const char* bd, const char* an, const char* fn );
        void init_w( const wchar_t* bd, const wchar_t* an, const wchar_t* fn );
        bool load();
        bool flush();
        void unload();
        void getbasepath();

    protected:
        void*           instance;
        std::string     inipath;
        std::string     appname;
        std::string     inifilename;
        bool            loaded;
};

#endif /// of __CFGLDR_H__

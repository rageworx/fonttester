#ifndef __STOOLS_H__
#define __STOOLS_H__

char*    Convert2MBCS( const wchar_t* str = NULL );
wchar_t* Convert2WCS( const char* str = NULL );

#endif /// of __STOOLS_H__

#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <vector>

#include "winMain.h"

#ifdef __linux__
    #include <signal.h>
#endif

#include <FL/platform.H>
#include "fl_imgtk.h"
#include <FL/Fl_Copy_Surface.H>
#include <FL/fl_draw.H>
#include <FL/fl_ask.H>
#if defined(USE_SYSPNG)
    #include <png.h>
#else
    #include <FL/images/png.h>
#endif /// of defined(USE_SYSPNG)

#include "resource.h"
#include "dtools.h"
#include "ltools.h"
#include "stools.h"

#include <FL/Fl_BMP_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Native_File_Chooser.H>

#include "fl_pngwriter.h"
#include "fl_jpgwriter.h"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME            "Font Tester"
#define DEF_WIDGET_FSZ          16
#if !defined(_WIN32)
#define DEF_WIDGET_FNT          FL_HELVETICA
#else
#define DEF_WIDGET_FNT          FL_FREE_FONT
#endif

#define DEF_WIN_WIDTH           800
#define DEF_WIN_HEIGHT          500
#define DEF_WIN_MIN_WIDTH       400
#define DEF_WIN_MIN_HEIGHT      300
#define DEF_WIN_COLOR_BG        0x33333300
#define DEF_WIN_COLOR_FG        0xBBBBBB00
#define DEF_INPUT_TEXT_BG       0x40404000
#define DEF_INPUT_TEXT_FG       0xAAAAAA00
#define DEF_NOTICE_COL_FG       0xEE997700
#define DEF_CURSOR_COL          0x99333300
#define DEF_SELECTION_COL       0xAAAAAA00

#define DEF_DEFAULT_USER_TEXT  "Load or drag font below area and type any text here.\n"\
                               "You can write multiple lines."

#define __MIN(a,b)              (((a)<(b))?(a):(b))
#define __MAX(a,b)              (((a)>(b))?(a):(b))

////////////////////////////////////////////////////////////////////////////////

string resourcebase;

bool getResource( const char* scheme, uchar** buff, unsigned* buffsz )
{
#ifdef _WIN32
    wchar_t convscheme[80] = {0,};

    fl_utf8towc( scheme, strlen(scheme), convscheme, 80 );

    HRSRC rsrc = FindResource( NULL, convscheme, RT_RCDATA );
    if ( rsrc != NULL )
    {
        *buffsz = SizeofResource( NULL, rsrc );
        if ( *buffsz > 0 )
        {
            HGLOBAL glb = LoadResource( NULL, rsrc );
            if ( glb != NULL )
            {
                void* fb = LockResource( glb );
                if ( fb != NULL )
                {
                    uchar* cpbuff = new uchar[ *buffsz ];
                    if ( cpbuff != NULL )
                    {
                        memcpy( cpbuff, fb, *buffsz );

                        *buff = cpbuff;

                        UnlockResource( glb );
                        return true;
                    }

                    UnlockResource( glb );
                }
            }
        }
    }
#elif defined(__APPLE__)
    string rescomb = resourcebase;
    rescomb += "/";
    rescomb += scheme;
    rescomb += ".png";
#ifdef DEBUG
    printf("Load resource : %s : ", rescomb.c_str() );
#endif
    FILE* fp = fopen( rescomb.c_str(), "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0L, SEEK_END );
        size_t flen = ftell( fp );
        fseek( fp, 0L, SEEK_SET );

        if ( flen > 0 )
        {
            uchar* cpbuff = new uchar[ flen ];
            if ( cpbuff != NULL )
            {
                fread( cpbuff, 1, flen, fp );
                fclose( fp );
                *buffsz = flen;
                *buff = cpbuff;
#ifdef DEBUG
                printf("Ok.\n");
#endif
                return true;
            }
        }
        else
        {
            fclose( fp );
        }
    }
#ifdef DEBUG
    printf("Failure.\n");
#endif

#else /// --> Linux
    string rescomb = resourcebase;
    rescomb += "/";
    rescomb += scheme;
    rescomb += ".png";
#ifdef DEBUG
    printf("Load resource : %s : ", rescomb.c_str() );
#endif
    FILE* fp = fopen( rescomb.c_str(), "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0L, SEEK_END );
        size_t flen = ftell( fp );
        fseek( fp, 0L, SEEK_SET );

        if ( flen > 0 )
        {
            uchar* cpbuff = new uchar[ flen ];
            if ( cpbuff != NULL )
            {
                int fr = fread( cpbuff, 1, flen, fp );
                fclose( fp );
                *buffsz = flen;
                *buff = cpbuff;
#ifdef DEBUG
                printf("Ok.\n");
#endif
                return true;
            }
        }
        else
        {
            fclose( fp );
        }
    }

#endif /// of _WIN32

    return false;
}

Fl_RGB_Image* createResImage( const char* scheme )
{
    if ( scheme != NULL )
    {
        uchar* buff = NULL;
        unsigned buffsz = 0;

        if ( getResource( scheme, &buff, &buffsz ) == true )
        {
            Fl_RGB_Image* retimg = new Fl_PNG_Image( scheme, buff, buffsz );
            delete[] buff;

            return retimg;
        }
    }

    return NULL;
}

const char* _ftoa( float f )
{
    static char fstr[80] = {0};
    sprintf( fstr, "%.2f", f );
    return fstr;
}

////////////////////////////////////////////////////////////////////////////////

WMain::WMain( int argc, char** argv )
 :  _argc( argc ),
    _argv( argv ),
    window( NULL ),
    frender( NULL ),
    fontbuff( NULL ),
    imgRender( NULL ),
    cfgldr( NULL )
{
    // init FLTK lock mechanism once.
    Fl::lock();

    // Load configurations ...
    cfgldr = new ConfigLoader( (const char*)NULL, "fonttester" );

    // creates somethings ..
    getEnvironments();
    createComponents();

#ifdef _WIN32
    HICON \
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );
    HICON \
    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );

    if ( window != NULL )
    {
        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_BIG,
                     (LPARAM)hIconWindowLarge );

        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_SMALL,
                     (LPARAM)hIconWindowSmall );
    }

#elif defined(__linux__)
    Fl_RGB_Image* imgIcon = createResImage( "fonttester" );
    if( imgIcon != NULL )
    {
        window->icon( imgIcon );

        fl_imgtk::discard_user_rgb_image( imgIcon );
    }
#endif /// of _WIN32
}

WMain::~WMain()
{
    if ( cfgldr != NULL )
    {        
        delete cfgldr;
    }
    
    if ( imgRender != NULL )
    {
        rndViewer->unloadimage();
        imgRender = NULL;
    }
        
    if ( frender != NULL )
    {
        delete frender;                 
    }
    
    if ( fontbuff != NULL )
    {
        delete[] fontbuff;
    }
}

int WMain::Run()
{
    if ( window != NULL )
    {
        return Fl::run();
    }

    return 0;
}

void WMain::WidgetCall( Fl_Widget* w )
{
    if ( w == window )
    {
        if ( cfgldr != NULL )
        {
            int d = Fl::screen_num( window->x(), window->y() );
            bool retb = cfgldr->SetWindowPos( window->x(), window->y(), 
                                              window->w(), window->h(), 
                                              d );
#ifdef DEBUG
            if ( retb == false )
                fprintf( stderr, "cannot write ini!\n" );
#endif
            retb = cfgldr->SetFontSize( (unsigned) inpFontSize->value() );
#ifdef DEBUG
            if ( retb == false )
                fprintf( stderr, "cannot write ini!\n" );
#endif
            if ( inpText->value() != NULL )
            {
                const char* txt = inpText->value();
                retb = cfgldr->SetUserText( txt );
#ifdef DEBUG
                if ( retb == false )
                    fprintf( stderr, "cannot write ini!\n" );
#endif
            }
        }
        
        window->hide();
    }
    
    if ( w == btnLoadFont )
    {
        btnLoadFont->deactivate();
        loadFont();
        btnLoadFont->activate();
        return;
    }
    
    if ( w == btnBold )
    {
        btnBold->deactivate();
        btnRender->do_callback();
        btnBold->activate();
    }
    
    if ( w == btnItalic )
    {
        btnItalic->deactivate();
        btnRender->do_callback();
        btnItalic->activate();
        return;
    }

    if ( w == btnRender )
    {       
        btnRender->deactivate();
        
        if ( frender != NULL )
        {
            const char* refS = inpText->value();
            
            if ( refS != NULL )
            {
                // sperating lines ...
#ifdef DEBUG
                printf( "#DEBUG: test line source :\n%s\n==========================\n",
                        refS );
                fflush( stdout );
#endif
                vector< string > lines = split( refS, '\n' );
#ifdef DEBUG
                printf( "#DEBUG: splited lines: %u\n", lines.size() );
                fflush( stdout );
#endif
                if ( lines.size() == 0 )
                    return;
                
                FLFTRender::Rect totalRect = {0};
                vector< Fl_RGB_Image* > imgRItems;

                for ( unsigned cnt=0; cnt<lines.size(); cnt++ )
                {
                    size_t decodelen = lines[cnt].size();
                    
                    if ( decodelen > 0 )
                    {
                        wchar_t* decodedstr = new wchar_t[ decodelen + 1 ];
                        if ( decodedstr != NULL )
                        {
                            memset( decodedstr, 0, ( decodelen + 1 ) * sizeof(wchar_t) );
#ifdef DEBUG
                            printf( "#DEBUG: source string => %s\n", lines[cnt].c_str() );
                            fflush( stdout );
#endif
                            if ( fl_utf8towc( lines[cnt].c_str(), decodelen, decodedstr, decodelen + 1 ) == 0 )
                            {
#ifdef DEBUG
                                printf( "#DEBUG: converting : mbcs to wchar_t\n" );
                                fflush( stdout );
#endif
                                wchar_t* teststr = Convert2WCS( lines[cnt].c_str() );
                                if ( teststr != NULL )
                                {
                                    wcsncpy( decodedstr, teststr, decodelen );
                                }
                            }
#ifdef DEBUG
                            wprintf( L"#DEBUG: converted wide string => %S\n", decodedstr );
                            fflush( stdout );
#endif
                            unsigned fontsize = inpFontSize->value();
                            frender->FontSize( fontsize );
                            frender->FontColor( 0xFFFFFFFF );
                            frender->Bold( false );
                            frender->Italic( false );
                            
                            if ( btnBold->value() > 0 )
                                frender->Bold( true );
                            
                            if ( btnItalic->value() > 0 )
                                frender->Italic( true );
                            
                            // Get measured rectangle.
                            FLFTRender::Rect rect = {0};
                            frender->MeasureText( decodedstr, rect );
#ifdef DEBUG
                            printf( "#DEBUG: measured rect -> %u,%u,%u,%u\n", rect.x, rect.y, rect.w, rect.h );
                            fflush( stdout );
#endif
                            
                            // check rect 
                            if ( ( rect.w == 0 ) || ( rect.h == 0 ) || ( (long)rect.h < 0 ) )
                            {
#ifdef DEBUG
                                printf( "Strange! measure text result : width = %ld, height = %ld\n",
                                        (long)rect.w, (long)rect.h );
                                fflush( stdout );
#endif /// of DEBUG
                                btnRender->activate();
                                return;
                            }

                            unsigned marginL = 10;
                            unsigned marginT = 10;

                            // put more margin
                            rect.w += marginL * 2;
                            rect.h += marginT * 2;

                            Fl_RGB_Image* imgItem = fl_imgtk::makeanempty( rect.w, rect.h, 4, 0x1010107F );
                            if ( imgItem != NULL )
                            {
                                // Original boundary --
                                fl_imgtk::draw_rect( imgItem, 
                                                     rect.x, 
                                                     rect.y, 
                                                     rect.x + rect.w - 1, 
                                                     rect.y + rect.h - 1 , 
                                                     0xFF33336F );
                                                     
                                // Horizon base.
                                fl_imgtk::draw_line( imgItem,
                                                     rect.x + marginL,
                                                     rect.y + marginT + fontsize - 1,
                                                     rect.x + rect.w - marginL - 1, 
                                                     rect.y + marginT + fontsize - 1,
                                                     0xFF33336F );

                                // Expended boundary --
                                fl_imgtk::draw_rect( imgItem,
                                                     rect.x + marginL,
                                                     rect.y + marginT,
                                                     rect.x + rect.w - ( marginL * 2 ) - 1, 
                                                     rect.y + rect.h - ( marginT * 2 ) - 1, 
                                                     0x33FF336F );

                                if ( frender->RenderText( imgItem, 10, 10, decodedstr, &rect ) == true )
                                {
                                    // actual boundary --
                                    fl_imgtk::draw_rect( imgItem,
                                                         rect.x, 
                                                         rect.y, 
                                                         rect.x + rect.w - 1, 
                                                         rect.y + rect.h - 1 , 
                                                         0xFF33FF6F );
                                }
                                
                                imgRItems.push_back( imgItem );
                                
                                totalRect.w  = __MAX( totalRect.w, imgItem->w() );
                                totalRect.h += imgItem->h();
                            }
                                                        
                            delete[] decodedstr;
                        }
                    } /// of if ( decodelen > 0 )
                } /// of for () ...
            
            
                Fl::lock();
                
                if ( imgRender != NULL )
                {
                    rndViewer->unloadimage();
                    imgRender = NULL;
                }

                unsigned col_bg = rndViewer->color() |  0x000000FF;

                imgRender = fl_imgtk::makeanempty( totalRect.w, totalRect.h, 4, col_bg );
                if ( imgRender != NULL )
                {
                    unsigned putY = 0;
                    
                    for ( unsigned cnt=0; cnt<imgRItems.size(); cnt++ )
                    {
                        fl_imgtk::drawonimage( imgRender, imgRItems[cnt], 0, putY );
                        putY += imgRItems[cnt]->h();
                    }
                }

                for ( unsigned cnt=0; cnt<imgRItems.size(); cnt++ )
                {
                    fl_imgtk::discard_user_rgb_image( imgRItems[cnt] );
                }
                
                imgRItems.clear();

                rndViewer->image( imgRender, 1 );
                
                Fl::unlock();
            }
        }
        
        btnRender->activate();
        return;
    }
}

void WMain::OnMouseMove( void* w, int x, int y, bool inside )
{

}

void WMain::OnMouseClick( void* w, int x, int y, unsigned btn )
{
    if ( btn == FL_BUTTON3 )
    {
    }
}

void WMain::OnKeyPressed( void* w, unsigned short k, int s, int c, int a )
{
#ifdef DEBUG
    printf("OnKeyPressed!\n");
#endif // DEBUG
}

void WMain::OnDropFiles( void* w, const char* files )
{
#ifdef DEBUG
    printf("OnDropFiles : %s\n", files);
#endif // DEBUG

    size_t decodelen = strlen( files );
    if ( decodelen > 0 )
    {
#ifdef SUPPORT_WCHAR
        wchar_t* decodedstr = new wchar_t[ decodelen + 1 ];
        if ( decodedstr != NULL )
        {
            fl_utf8towc( files, strlen( files ),
                         decodedstr, decodelen + 1 );

            wstring tmpfiles = decodedstr;

            delete[] decodedstr;

            vector<wstring> tmpfilelist;

            tmpfilelist = split( tmpfiles, L'\n' );
#else
        const char* decodedstr = files;
        if ( decodedstr != NULL )
        {
            vector<string> tmpfilelist;
            string tmpfiles = decodedstr;

            tmpfilelist = split( tmpfiles, '\n' );
#endif /// of SUPPORT_WCHAR
            if ( tmpfilelist.size() > 0 )
            {
#ifdef __linux__
                for( size_t cnt=0; cnt<tmpfilelist.size(); cnt++ )
                {
#ifdef DEBUG
                    printf( "splitted file path : %s -> ",
                            tmpfilelist[cnt].c_str() );
#endif /// of DEBUG

                    decodeURLpath( tmpfilelist[cnt] );
#ifdef DEBUG
                    printf( "%s\n",
                            tmpfilelist[cnt].c_str() );
#endif /// of DEBUG
               }
#endif /// of  __linux__

#ifdef DEBUG
                printf( "Loading font file %S ...\n",
                        tmpfilelist[0].c_str() );
#endif /// of DEBUG             
                if ( frender != NULL )
                {
                    delete frender;                 
                }
                
                if ( fontbuff != NULL )
                {
                    delete[] fontbuff;
                }
                
                // Load Buffer ...
#ifdef SUPPORT_WCHAR
                FILE* fp = _wfopen( tmpfilelist[0].c_str(), L"rb" );
#else
                FILE* fp = fopen( tmpfilelist[0].c_str(), "rb" );
#endif
                if ( fp != NULL )
                {
                    fseek( fp, 0L, SEEK_END );
                    unsigned fsz = ftell( fp );
                    rewind( fp );
                    if ( fsz > 0 )
                    {
                        fontbuff = new unsigned char[ fsz ];
                        
                        if ( fontbuff == NULL )
                        {
                            fclose( fp );
                            return ;
                        }
                        
                        int fr = fread( fontbuff, 1, fsz, fp );
                        fclose( fp );
                        
                        frender = new FLFTRender( fontbuff, fsz );
                        if ( frender != NULL )
                        {
                            if ( frender->FontLoaded() == true )
                            {
                                fontName  = frender->FamilyName();
                                fontName += " / ";
                                fontName += frender->StyleName();
                                boxFontName->label( fontName.c_str() );
                                boxFontName->redraw();
                            }
                        }

                        delete[] fontbuff;
                        fontbuff = NULL;
                        
                        btnRender->do_callback();
                    }
                }
                
            }
        }
    }
}

void WMain::OnDrawCompleted()
{
}

void WMain::getEnvironments()
{
#ifdef _WIN32
    pathHome        = _wgetenv( L"USERPROFILE" );
    pathSystemBase  = _wgetenv( L"SYSTEMROOT" );
    pathUserData    = _wgetenv( L"LOCALAPPDATA" );
    pathUserRoaming = _wgetenv( L"APPDATA" );
#ifdef DEBUG
    wprintf( L"pathHome        : %S\n", pathHome.c_str() );
    wprintf( L"pathSystemBase  : %S\n", pathSystemBase.c_str() );
    wprintf( L"pathUserData    : %S\n", pathUserData.c_str() );
    wprintf( L"pathUserRoaming : %S\n", pathUserRoaming.c_str() );
#endif // DEBUG
#elif defined(__APPLE__)
    resourcebase = _argv[0];
    size_t fpos = resourcebase.find_last_of('/');
    if ( fpos != string::npos )
    {
        resourcebase = resourcebase.substr( 0, fpos );

        fpos = resourcebase.find_last_of('/');

        if ( fpos != string::npos )
        {
            resourcebase = resourcebase.substr( 0, fpos );
            resourcebase += "/Resources";
        }
    }

    pathHome        = getenv( "HOME" );
    pathUserData    = pathHome;
    pathUserRoaming = pathHome;
#else /// must be Linux
    resourcebase = "/usr/local/share/images/fonttester";

    if ( access( resourcebase.c_str(), 0 ) != 0 )
    {
        resourcebase = _argv[0];
        size_t fpos = resourcebase.find_last_of('/');
        if ( fpos != string::npos )
        {
            resourcebase = resourcebase.substr( 0, fpos );
            resourcebase += "/res";
        }
    }

    pathHome        = getenv( "HOME" );
    pathUserData    = pathHome;
    pathUserRoaming = pathHome;

#ifdef DEBUG
    printf("#DEBUG.PATHES\n");
    printf("HOME: %s\n", pathHome.c_str());
    printf("resourcebase: %s\n",resourcebase.c_str());
    #endif
#endif /// of _WIN32, __APPLE__ and __linux__
}

void WMain::setdefaultwintitle()
{
    memset( wintitlestr, 0, MAX_WINTITLE_LEN );
    sprintf( wintitlestr, "%s v.%s", DEF_APP_NAME, APP_VERSION_STR );
}

void WMain::createComponents()
{
    setdefaultwintitle();
    
    unsigned ww = DEF_WIN_WIDTH;
    unsigned wh = DEF_WIN_HEIGHT;

    window = new Fl_Double_Window( ww, wh, wintitlestr );
    if ( window != NULL )
    {
        window->color( DEF_WIN_COLOR_BG );
        window->labelfont( DEF_WIDGET_FNT );
        window->labelsize( DEF_WIDGET_FSZ );
        window->labelcolor( DEF_WIN_COLOR_FG );

        // continue to child components ...
        window->begin();

        int put_x = 0;
        int put_y = 0;
        int div_h = wh * 0.3f;
        int div_w = ww * 0.8f;

        grpDiv0 = new Fl_Group( put_x, put_y, ww, 32 );
        if ( grpDiv0 != NULL )
        {
            grpDiv0->begin();
            
            grpDiv01 = new Fl_Group( grpDiv0->x(), grpDiv0->y(), grpDiv0->w() * 0.7f, grpDiv0->h() );
            if ( grpDiv01 != NULL )
            {
                grpDiv0->begin();
                
                int px = 2;
                int py = 2;
                
                btnLoadFont = new Fl_Button( px , py , 100, 28, "&Load Font" );
                if ( btnLoadFont != NULL )
                {
                    btnLoadFont->color( window->color(), fl_lighter( window->color() ) );
                    btnLoadFont->labelcolor( window->labelcolor() );
                    btnLoadFont->labelfont( window->labelfont() );
                    btnLoadFont->labelsize( window->labelsize() );
                    btnLoadFont->callback( WMain::WidgetCB, this );
                    
                    px += btnLoadFont->w() + 5;
                }
                
                btnBold = new Fl_Button( px, py, 28, 28, "B" );
                if ( btnBold != NULL )
                {
                    btnBold->type( FL_TOGGLE_BUTTON );
                    btnBold->color( window->color(), fl_lighter( window->color() ) );
                    btnBold->labelcolor( window->labelcolor() );
                    btnBold->labelfont( FL_TIMES_BOLD );
                    btnBold->labelsize( window->labelsize() );
                    btnBold->callback( WMain::WidgetCB, this );
                    
                    px += btnBold->w() + 5;
                }
                
                btnItalic = new Fl_Button( px, py, 28, 28, "I" );
                if ( btnItalic != NULL )
                {
                    btnItalic->type( FL_TOGGLE_BUTTON );
                    btnItalic->color( window->color(), fl_lighter( window->color() ) );
                    btnItalic->labelcolor( window->labelcolor() );
                    btnItalic->labelfont( FL_TIMES_ITALIC );
                    btnItalic->labelsize( window->labelsize() );
                    btnItalic->callback( WMain::WidgetCB, this );
                    
                    px += btnBold->w() + 5;
                }                
            
                inpFontSize = new Fl_Value_Input( px + 50, py, 50, 28, "size:" );
                if ( inpFontSize != NULL )
                {
                    inpFontSize->color( 0 );
                    inpFontSize->range( 8, 100 );
                    inpFontSize->type( FL_INT_INPUT );
                    inpFontSize->textfont( FL_COURIER );
                    inpFontSize->textcolor( DEF_INPUT_TEXT_FG );
                    inpFontSize->textsize( window->labelsize() );
                    inpFontSize->labelcolor( window->labelcolor() );
                    inpFontSize->labelfont( window->labelfont() );
                    inpFontSize->labelsize( window->labelsize() );
                    
                    unsigned fsz = 40;
                    
                    if ( cfgldr != NULL )
                    {
                        cfgldr->GetFontSize( fsz );
                    }
                    
                    inpFontSize->value( fsz );
                    
                    px += inpFontSize->w() + 50 + 5;
                }
                                
                grpDiv0->end();
            }
            
            grpDiv02 = new Fl_Group( grpDiv0->w() * 0.7f, grpDiv0->y(), grpDiv0->w() * 0.3f, grpDiv0->h() );
            if ( grpDiv02 != NULL )
            {
                grpDiv02->begin();
                
                boxFontName = new Fl_Box( grpDiv02->x() + 1, grpDiv02->y(), 
                                          grpDiv02->w() - 4, grpDiv02->h(), "no font" );
                if ( boxFontName != NULL )
                {
                    boxFontName->box( FL_FLAT_BOX );
                    // need to use flag of "FL_ALIGN_INSIDE" ?
                    boxFontName->align( FL_ALIGN_INSIDE | FL_ALIGN_RIGHT | FL_ALIGN_WRAP );
                    boxFontName->color( window->color() );
                    boxFontName->labelcolor( DEF_NOTICE_COL_FG );
                    boxFontName->labelfont( window->labelfont() );
                    boxFontName->labelsize( window->labelsize() - 2 );
                }
                
                grpDiv02->end();
                grpDiv0->resizable( grpDiv02 );
            }
            
            grpDiv0->end();
            put_y += 32;
        }

        grpDiv1 = new Fl_Group( put_x, put_y, ww, div_h );
        if ( grpDiv1 != NULL )
        {
            grpDiv1->begin();
            
            grpDiv11 = new Fl_Group( grpDiv1->x(), grpDiv1->y(), div_w, grpDiv1->h() );
            if ( grpDiv11 != NULL )
            {
                grpDiv11->begin();
                
                inpText = new Fl_Multiline_Input( grpDiv11->x() + 1, grpDiv11->y() + 1, 
                                                  grpDiv11->w() - 2, grpDiv11->h() - 2 );
                if ( inpText != NULL )
                {
                    inpText->color( DEF_INPUT_TEXT_BG );
                    inpText->labelfont( DEF_WIDGET_FNT );
                    inpText->textfont( DEF_WIDGET_FNT );
                    inpText->textcolor( DEF_INPUT_TEXT_FG );
                    inpText->textsize( window->labelsize() );
                    inpText->cursor_color( DEF_CURSOR_COL );
                    inpText->selection_color( DEF_SELECTION_COL );
                    
                    char* usrtxt = NULL;
                    
                    if ( cfgldr != NULL )
                    {
                        if ( cfgldr->GetUserText( usrtxt ) == true )
                        {
                            inpText->value( usrtxt );
                        }
                    }
                    
                    if ( usrtxt == NULL )
                    {
                        inpText->value( DEF_DEFAULT_USER_TEXT );
                    }
                    else
                    {
                        delete[] usrtxt;
                    }
                }
                
                grpDiv11->end();
            }
            
            grpDiv12 = new Fl_Group( grpDiv1->x() + div_w, grpDiv1->y(), 
                                     ww - div_w - grpDiv1->x(), grpDiv1->h() );
            if ( grpDiv12 != NULL )
            {
                grpDiv12->begin();
                
                btnRender = new Fl_Button( grpDiv12->x() + 2, grpDiv12->y() + 2, 
                                           grpDiv12->w() - 4, grpDiv12->h() - 4,
                                           "&Render" );
                if ( btnRender != NULL )
                {
                    btnRender->box( FL_THIN_UP_BOX );
                    btnRender->color( window->color(), fl_lighter( window->color() ) );
                    btnRender->labelfont( window->labelfont() );
                    btnRender->labelcolor( window->labelcolor() );
                    btnRender->labelsize( window->labelsize() * 2 );
                    btnRender->callback( WMain::WidgetCB, this );
                }
                
                grpDiv12->end();
            }
            
            grpDiv1->resizable( grpDiv11 );
            grpDiv1->end();
            
            put_y += grpDiv1->h();
        }
        
        grpDiv2 = new Fl_Group( put_x, put_y, ww, wh - put_y - 1 );
        if ( grpDiv2 != NULL )
        {
            grpDiv2->begin();
                        
            rndViewer = new Fl_RenderViewer( grpDiv2->x(), grpDiv2->y(), grpDiv2->w(), grpDiv2->h() );
            if ( rndViewer != NULL )
            {
                rndViewer->color( window->color() );
                rndViewer->notifier( this );
            }

            grpDiv2->end();
            window->resizable( grpDiv2 );
            window->size_range( DEF_WIN_MIN_WIDTH, DEF_WIN_MIN_HEIGHT );
        }
        
        window->end();
        window->callback( WMain::WidgetCB, this );
        
        // check config
        if ( cfgldr != NULL )
        {
            int wx = 0;
            int wy = 0;
            int ww = 0;
            int wh = 0;
            int wd = 0;
            
            if ( cfgldr->GetWindowPos( wx, wy, ww, wh, wd ) == true )
            {
                window->resize( wx, wy, ww, wh );
            }
        }
        
        window->show();
        
        if ( inpText != NULL )
        {
            inpText->take_focus();
        }
    }
}

void WMain::loadFont()
{
    Fl_Native_File_Chooser nFC;

    nFC.title( "Select font file to load." );
    nFC.type( Fl_Native_File_Chooser::BROWSE_FILE );
    nFC.options( Fl_Native_File_Chooser::PREVIEW );
#if defined(__APPLE__)||defined(__linux__)
    nFC.filter( "Supported Font\t*.{ttf,ttc,otf}\n"
                "True type\t*.{ttf,ttc}\n"
                "Open true type\t*.otf" );
#else
    nFC.filter( "Supported Font\t*.ttf;*.ttc;*.otf\n"
                "True type\t*.ttf;*.ttc\n"
                "Open true type\t*.otf" );
#endif /// of __APPLE__
    nFC.preset_file( getenv( "HOME" ) );

    char refconvpath[1024] = {0,};

#ifdef SUPPORT_WCHAR
    if ( pathFont.size() > 0 )
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathFont.c_str(), pathFont.size() );
    }
    else
    {
        fl_utf8fromwc( refconvpath, 1024,
                       pathFont.c_str(), pathFont.size() );
    }
#else
    strcpy( refconvpath, pathHome.c_str() );
#endif
    nFC.directory( refconvpath );

    int retVal = nFC.show();

    if ( retVal == 0 )
    {
        string loadfn = nFC.filename();

        OnDropFiles( rndViewer, loadfn.c_str() );
    }
}

bool WMain::save2png( Fl_RGB_Image* img, const char* fname )
{
    if ( ( img != NULL ) && ( fname != NULL ) )
    {
        if ( img->d() < 3 )
            return false;

        if ( access( fname, F_OK ) == 0 )
        {
            if ( unlink( fname ) != 0 )
                return false; /// failed to remove file !
        }

        FILE* fp = fopen( fname, "wb" );
        if ( fp != NULL )
        {
            png_structp png_ptr     = NULL;
            png_infop   info_ptr    = NULL;
            png_bytep   row         = NULL;

            png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING,
                                               NULL,
                                               NULL,
                                               NULL );
            if ( png_ptr != NULL )
            {
                info_ptr = png_create_info_struct( png_ptr );
                if ( info_ptr != NULL )
                {
                    if ( setjmp( png_jmpbuf( (png_ptr) ) ) == 0 )
                    {
                        char tmpstr[1024] = {0,};

                        int png_c_type = PNG_COLOR_TYPE_RGB;

                        if ( img->d() == 4 )
                        {
                            png_c_type = PNG_COLOR_TYPE_RGBA;
                        }

                        png_init_io( png_ptr, fp );
                        // Let compress image with libz
                        //  changing compress levle 8 to 7 for speed.
                        png_set_compression_level( png_ptr, 7 );
                        png_set_IHDR( png_ptr,
                                      info_ptr,
                                      img->w(),
                                      img->h(),
                                      8,
                                      png_c_type,
                                      PNG_INTERLACE_NONE,
                                      PNG_COMPRESSION_TYPE_BASE,
                                      PNG_FILTER_TYPE_BASE);

                        png_write_info( png_ptr, info_ptr );

                        const uchar* buff = (const uchar*)img->data()[0];

                        unsigned rowsz = img->w() * sizeof( png_byte ) * img->d();

                        row = (png_bytep)malloc( rowsz );
                        if ( row != NULL )
                        {
                            int bque = 0;

                            for( int y=0; y<img->h(); y++ )
                            {
                                memcpy( row, &buff[bque], rowsz );
                                bque += rowsz;

                                png_write_row( png_ptr, row );
                            }

                            png_write_end( png_ptr, NULL );

                            fclose( fp );
                            free(row);

                            window->label( wintitlestr );
                        }

                        png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

                        return true;
                    }
                    png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                }
                png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
            }
        }
    }

    return false;
}


#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <minIni.h>
#include <FL/fl_utf8.h>

#include "cfgldr.h"
#include "stools.h"
#include "base64.h"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEFAULT_FNAME   "fonttester.cfg"
#define DEFAULT_INI     "[WINDOW]\nX=20\nY=20\nW=800\nH=500\nD=0\n"\
                        "[USER]\nfontsize=40\nworkingpath=\nusertexts=\n";

#define INSTANCECAST(_x_)   minIni* _x_ = (minIni*)instance

////////////////////////////////////////////////////////////////////////////////

static bool createdefaultini( const char* fpath )
{
    FILE* fp = fopen( fpath, "wb" );
    if ( fp != NULL )
    {
        char refstr[] = DEFAULT_INI;
        fwrite( refstr, 1, strlen( refstr ), fp );
        fclose( fp );
        
        return true;
    }
#ifdef DEBUG    
    else
    {
        printf( "Failed to create INI !\n" );
    }
#endif /// of DEBUG

    return false;
}


////////////////////////////////////////////////////////////////////////////////

ConfigLoader::ConfigLoader( const char* bd, const char* an, const char* fn )
 : instance( NULL ),
   loaded( false )
{
    init_a( bd, an ,fn );
}

ConfigLoader::ConfigLoader( const wchar_t* bd, const wchar_t* an, const wchar_t* fn )
 : instance( NULL ),
   loaded( false )
{
    init_w( bd, an, fn );
}

ConfigLoader::~ConfigLoader()
{
    if ( loaded == true )
    {
        flush();
        unload();
    }
}

bool ConfigLoader::GetWindowPos( int &x, int &y, int &w, int &h, int &d )
{
    if ( loaded == false )
        return false;

    INSTANCECAST( pIni );
    
    if ( pIni != NULL )
    {
        x = pIni->geti( "WINDOW", "X", 10 );
        y = pIni->geti( "WINDOW", "Y", 10 );
        w = pIni->geti( "WINDOW", "W", 800 );
        h = pIni->geti( "WINDOW", "H", 500 );
        d = pIni->geti( "WINDOW", "D", 0 );
        
        return true;
    }
    
    return false;
}

bool ConfigLoader::SetWindowPos( int x, int y, int w, int h, int d )
{
    if ( loaded == false )
        return false;

    INSTANCECAST( pIni );
    
    if ( pIni != NULL )
    {
        pIni->put( "WINDOW", "X", x );
        pIni->put( "WINDOW", "Y", y );
        pIni->put( "WINDOW", "W", w );
        pIni->put( "WINDOW", "H", h );
        pIni->put( "WINDOW", "D", d );
        
        return true;
    }
    
    return false;
}

bool ConfigLoader::GetFontSize( unsigned &sz )
{
    if ( loaded == false )
        return false;

    INSTANCECAST( pIni );
    
    if ( pIni != NULL )
    {
        sz = pIni->geti( "USER", "fontsize", 0 );
        
        if ( sz == 0 )
            sz = 40;
        
        return true;
    }
    
    return false;
}

bool ConfigLoader::SetFontSize( unsigned sz )
{
    if ( loaded == false )
        return false;

    INSTANCECAST( pIni );
    
    if ( pIni != NULL )
    {
        pIni->put( "USER", "fontsize", (int)sz );
        
        return true;
    }
    
    return false;
}

bool ConfigLoader::GetUserText( char* &u8t )
{
    if ( loaded == false )
        return false;
            
    INSTANCECAST( pIni );
        
    if ( pIni != NULL )
    {
        string tmp = pIni->gets( "USER", "usertexts" );
        if ( tmp.size() > 0 )
        {
            // convert to normal text.
            unsigned detsz = tmp.size();
            if ( detsz == 0 )
                return false;
                        
            unsigned char* decoded = new unsigned char[ detsz ];
            if ( decoded == NULL )
                return false;
            
            memset( decoded, 0, detsz );
                       
            int dlen = base64_decode( tmp.c_str(), decoded, tmp.size() );
            
            if ( ( decoded != NULL ) && ( dlen > 0 ) )
            {                
                u8t = new char[ dlen ];
                if ( u8t != NULL )
                {
                    memcpy( u8t, decoded, dlen );
                    
                    delete[] decoded;
                    
                    return true;
                }
                
                delete[] decoded;
            }
        }
    }

    return false;
}

bool ConfigLoader::SetUserText( const char* u8t )
{
    if ( loaded == false )
        return false;
            
    INSTANCECAST( pIni );
        
    if ( ( pIni != NULL ) && ( u8t != NULL ) )
    {
        int u8tlen = (int)strlen( u8t );
        char* encoded = NULL;
        int elen = base64_encode( u8t, u8tlen, &encoded );
        
        if ( ( encoded != NULL ) && ( elen > 0 ) )
        {
            pIni->put( "USER", "usertexts", encoded );
            
            delete[] encoded;
            
            return true;
        }
        
        if ( encoded != NULL )
            delete[] encoded;
    }

    return false;
}

bool ConfigLoader::GetLastPath( char* &path )
{
    if ( loaded == false )
        return false;
            
    INSTANCECAST( pIni );
        
    if ( pIni != NULL )
    {
        string tmpstr = pIni->gets( "USER", "workingpath", "." );
        
        path = strdup( tmpstr.c_str() );
        
        return true;
    }
    
    return false;
}

bool ConfigLoader::GetLastPath( wchar_t* &path )
{
    wstring retwstr;

    char* a_path = NULL;
    
    if ( GetLastPath( a_path ) == true )
    {
        wchar_t* conv = Convert2WCS( a_path );
    
        delete[] a_path;
    
        if ( conv != NULL )
        {
            path = wcsdup( conv );
            
            return true;
        }
    }
    
    return false;
}

bool ConfigLoader::SetLastPath( const char* path )
{
    INSTANCECAST( pIni );
    
    if ( pIni != NULL )
    {
        pIni->put( "USER", "workingpath", inipath.c_str() );
        
        return true;
    }
    
    return false;
}

bool ConfigLoader::SetLastPath( const wchar_t* path )
{
    return SetLastPath( Convert2MBCS( path ) );
}

void ConfigLoader::init_a( const char* bd, const char* an, const char* fn )
{
    inifilename = DEFAULT_FNAME;
    
    if ( an != NULL )
    {
        appname = an;
    }

    if ( ( bd != NULL ) && ( fn != NULL ) )
    {
        inipath = bd;
        appname = an;
        inifilename = fn;
#ifdef _WIN32
        inipath += "\\";
#else
        inipath += "/";    
#endif
        inipath += appname;
    }
    else
    {
        getbasepath();
    }

    if ( inipath.size() == 0 )
        return;

    char loadpath[512] = {0};
#ifdef _WIN32
    snprintf( loadpath, 512, "%s\\%s", inipath.c_str(), inifilename.c_str() );
#else
    snprintf( loadpath, 512, "%s/%s", inipath.c_str(), inifilename.c_str() );
#endif

    if ( access( loadpath, 0 ) != 0 )
    {
        if ( createdefaultini( loadpath ) == false )
            return;
    }

    minIni* pIni = new minIni( loadpath );
    
    if ( pIni != NULL )
    {
        instance = pIni;
        loaded = true;
    }
}

void ConfigLoader::init_w( const wchar_t* bd, const wchar_t* an, const wchar_t* fn )
{
    string _bd;
    string _an;
    string _fn;

    if ( bd != NULL )
        string _bd = Convert2MBCS( bd );
    
    if ( an != NULL )
        string _an = Convert2MBCS( an );
    
    if ( fn != NULL )
        string _fn = Convert2MBCS( fn );

    return init_a( _bd.c_str(), _an.c_str(), _fn.c_str() );
}

bool ConfigLoader::load()
{
    if ( loaded == true )
    {
        INSTANCECAST( pIni );
        
        return true;
    }
    
    return false;
}

bool ConfigLoader::flush()
{
    if ( loaded == true )
    {
        INSTANCECAST( pIni );
        
        return true;
    }
    
    return false;
}

void ConfigLoader::unload()
{
    if ( loaded == true )
    {
        minIni* pmini = (minIni*)instance;

        // INI has nothing to do here.        
        // then,
        delete pmini;
        instance = NULL;
        
        loaded = false;
    }
}

void ConfigLoader::getbasepath()
{
    char basepath[512] = {0};
    
#if defined(_WIN32)
    const wchar_t* ppath = _wgetenv( L"LOCALAPPDATA" );
    if ( ppath != NULL )
    {
        // convert to damn wchar_t to utf8.
        char convutf8[512] = {0};
        unsigned clen = fl_utf8fromwc( convutf8,
                                       512,
                                       ppath,
                                       wcslen( ppath ) );
        if ( clen > 0 )
        {
            snprintf( basepath, 512, "%s\\%s", convutf8, appname.c_str() );
        }
    }
#else
    const char* ppath = getenv( "HOME" );
    if ( ppath != NULL )
    {
        snprintf( basepath, 512, "%s/.%s", ppath, appname.c_str() );
    }
#endif

    if ( strlen( basepath ) > 0 )
    {
        inipath = basepath;
    }
    
    // check directory ...
    if ( access( inipath.c_str(), 0 ) != 0 )
    {
#ifdef _WIN32
        if ( mkdir( inipath.c_str() ) != 0 )
#else
        if ( mkdir( inipath.c_str(), 644 ) != 0 )
#endif
        {
            // failure case !
            inipath.clear();
        }
    }
}

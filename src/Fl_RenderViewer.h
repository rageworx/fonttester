#ifndef __Fl_RenderViewer_H__
#define __Fl_RenderViewer_H__

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>

class Fl_RenderViewerNotifier
{
    public:
        virtual void OnMouseMove( void* w, int x, int y, bool inside ) = 0;
        virtual void OnMouseClick( void* w, int x, int y, unsigned btn ) = 0;
        virtual void OnKeyPressed( void* w, unsigned short k, int s, int c, int a ) = 0;
        virtual void OnDropFiles( void* w, const char* files ) = 0;
        virtual void OnDrawCompleted() = 0;
};

class Fl_RenderViewer : public Fl_Box
{
    public:
        Fl_RenderViewer(int x,int y,int w,int h);
        virtual ~Fl_RenderViewer();

    public:
        // fittingtype 0: width, 1: height
        void image(Fl_RGB_Image* aimg, int fittingtype = 1);
        void unloadimage();
        void box(Fl_Boxtype new_box);
        void type(uchar t);

    public:
        Fl_RGB_Image* image()       { return imgsrc; }

    public:
        void      notifier( Fl_RenderViewerNotifier* p ) { _notifier = p; }
        Fl_RenderViewerNotifier* notifier()              { return _notifier; }

    public:
        int  imgw();
        int  imgh();

    public:
        // timer call
        void drawtimercheck();

    private:
        unsigned long long gettimenow();

    private:/// FLTK inherited ...
        int  handle( int event );
        void draw();
        void resize(int,int,int,int);

    private:
        float multiplier;
        bool  holddraw;

    private:
        Fl_RGB_Image*   imgsrc;
        Fl_RGB_Image*   imgNavigationBar;

    private:
        bool        isgeneratingimg;
        bool        drawruler;
        unsigned    rulerH;
        unsigned    rulerV;
        unsigned    rulerCfg;
        unsigned    rulerCbg;
        int         mouse_btn;
        int         shift_key;
        int         check_x;
        int         check_y;
        char        tltip[100];
        
    private:
        Fl_RenderViewerNotifier*  _notifier;
};


#endif /// of __Fl_RenderViewer_H__

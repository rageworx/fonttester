#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <string>

#if defined(__APPLE__)
    #include <sys/uio.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <sys/mman.h>
#elif defined(__linux__)
    //#include <sys/io.h>
#else
    #include <io.h>
#endif /// of __APPLE__

#include <vector>
#if defined(USE_SYSPNG)
	#include <png.h>
#else
	#include <FL/images/png.h>
#endif /// of defined(USE_SYSPNG)
#include "fl_pngwriter.h"
#include "fmemio.h"

using namespace std;

static pngwrite_progress_cb pngwrite_p_cb = NULL;

void fl_pngwriter_setcallback( pngwrite_progress_cb cb )
{
    pngwrite_p_cb = cb;
}

void fl_pngwriter_unsetcallback()
{
    pngwrite_p_cb = NULL;
}

bool fl_image_write_to_pngfile( Fl_Image* refimg, const char* refpath, int q  )
{
    if ( ( refimg == NULL ) || ( refpath == NULL ) )
        return false;

    if ( ( refimg->w() == 0 ) || ( refimg->h() == 0 ) || ( refimg->d() < 3 ) )
        return false;

    // compression quality check.
    if ( q < 2 )
        q = 2;
    else
    if ( q > 9 )
        q = 9;

    int i_d = refimg->d();
    int png_c_type = PNG_COLOR_TYPE_RGB;

    if ( i_d == 4 )
    {
        png_c_type = PNG_COLOR_TYPE_RGBA;
    }

    FILE* fp = fopen( refpath, "wb" );
    
    if ( fp == NULL )
        return false;
    
    png_structp png_ptr     = NULL;
    png_infop   info_ptr    = NULL;
    png_bytep   row         = NULL;
    
    png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
    if ( png_ptr != NULL )
    {
        info_ptr = png_create_info_struct( png_ptr );
        if ( info_ptr != NULL )
        {
            if ( setjmp( png_jmpbuf( (png_ptr) ) ) == 0 )
            {
                int mx = refimg->w();
                int my = refimg->h();
                
                png_init_io( png_ptr, fp );
				png_set_compression_level( png_ptr, q );
                png_set_IHDR( png_ptr,
                              info_ptr,
                              mx,
                              my,
                              8,
                              png_c_type,
                              PNG_INTERLACE_NONE,
                              PNG_COMPRESSION_TYPE_BASE,
                              PNG_FILTER_TYPE_BASE);
                
                png_write_info( png_ptr, info_ptr );
                
                const uchar* buff = (const uchar*)refimg->data()[0];

                unsigned rowsz = refimg->w() * i_d;
                row = new png_byte[ rowsz ];

                int bque = 0;
                int minur = refimg->h() / 50;

                for( int y=0; y<refimg->h(); y++ )
                {
                    memcpy( row, &buff[bque], rowsz );
                    bque += rowsz;

                    png_write_row( png_ptr, row );
                }
                
                png_write_end( png_ptr, NULL );
                
                delete[] row;
                fclose( fp );
                
                png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
                
                return true;
            }
        }
    }

    return false;
}

unsigned fl_array_write_to_pngbuffer( const uchar* refarray, int w, int h, int d, char** pngbuffer, int q )
{
    if ( ( refarray == NULL ) || ( w <= 0 ) || ( h <= 0 ) || ( d < 3 ) )
        return 0;
    
    // Open memory stream
    unsigned tmpbuffsz = w * h * d;
    uchar* tmpbuff = new uchar[ tmpbuffsz ];
    
    if ( tmpbuff == NULL )
        return 0;
    
    FILE* mfp = fmemopen( tmpbuff, tmpbuffsz, "wb" );
    
    if ( mfp == NULL )
        return 0;
    
    png_structp png_ptr     = NULL;
    png_infop   info_ptr    = NULL;
    png_bytep   row         = NULL;

    png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING,
                                       NULL,
                                       NULL,
                                       NULL );
    if ( png_ptr != NULL )
    {
        info_ptr = png_create_info_struct( png_ptr );
        if ( info_ptr != NULL )
        {
            if ( setjmp( png_jmpbuf( (png_ptr) ) ) == 0 )
            {
                char tmpstr[1024] = {0,};

                int png_c_type = PNG_COLOR_TYPE_RGB;

                if ( d == 4 )
                {
                    png_c_type = PNG_COLOR_TYPE_RGBA;
                }

                png_init_io( png_ptr, mfp );
                                
                // Configure compressing level with "q" 
                // (quality parameter )
                if ( q > 9 )
                {
                    q = 9;
                }
                else
                // Set compression level for minimal 2
                // in memory stream copy.
                if ( q < 2 ) 
                {
                    q = 2;
                }
                
                // Let compress image with libz
                //  changing compress levle 8 to 7 for speed.
                png_set_compression_level( png_ptr, q );
                png_set_IHDR( png_ptr,
                              info_ptr,
                              w,
                              h,
                              8,
                              png_c_type,
                              PNG_INTERLACE_NONE,
                              PNG_COMPRESSION_TYPE_BASE,
                              PNG_FILTER_TYPE_BASE);

                png_write_info( png_ptr, info_ptr );
                                
                const uchar* buff = (const uchar*)refarray;

                unsigned rowsz = w * d;
                row = new png_byte[ rowsz ];

                int bque = 0;
                int minur = h / 50;

                for( int y=0; y<h; y++ )
                {
                    memcpy( row, &buff[bque], rowsz );
                    bque += rowsz;

                    png_write_row( png_ptr, row );

                    // Display progress..
                    if ( pngwrite_p_cb != NULL )
                    {
                        pngwrite_p_cb( y * w, h * w );
                    }
                }

                png_write_end( png_ptr, NULL );

                delete[] row;

                png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

                if ( pngwrite_p_cb != NULL )
                {
                    // Send finalize (maximum size) callback proc.
                    pngwrite_p_cb( h * w, h * w );
                }
            }
            
            png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
        }
        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
        
        long realfsz = fseek( mfp, SEEK_END, 0 );
        fseek( mfp, SEEK_CUR, 0 );
        
        if ( realfsz > 0 )
        {
            *pngbuffer = new char[ realfsz ];

            if ( *pngbuffer != NULL )
            {
                memcpy( *pngbuffer, tmpbuff, realfsz );
                fclose( mfp );
                
                return realfsz;
            }
        }
        
        fclose( mfp );
    }

    return 0;
}

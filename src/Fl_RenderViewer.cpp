#include <sys/time.h>
#include <cmath>
#include <vector>
#include <algorithm>

#ifdef USE_OMP
#include <omp.h>
#endif /// of USE_OMP

#include <FL/Fl_Copy_Surface.H>
#include <FL/fl_draw.H>
#if defined(USE_SYSPNG)
    #include <png.h>
#else
    #include <FL/images/png.h>
#endif /// of defined(USE_SYSPNG)
#include "Fl_RenderViewer.h"
#include <FL/Fl_Tooltip.H>
#include "fl_imgtk.h"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

Fl_RenderViewer::Fl_RenderViewer(int x,int y,int w,int h)
 : Fl_Box( x, y, w, h ),
   imgsrc( NULL ),
   _notifier( NULL ),
   holddraw( false ),
   drawruler( true ),
   rulerH( 10 ),
   rulerV( 10 ),
   rulerCfg( 0xCCCCCCFF ),
   rulerCbg( 0x555555FF )
{
    Fl_Box::box( FL_NO_BOX );
    color( FL_BLACK );
    type( 0 );
    labelcolor( FL_WHITE );
    labelsize( 12 );
    
    redraw();
}

Fl_RenderViewer::~Fl_RenderViewer()
{
    unloadimage();
}

void Fl_RenderViewer::image(Fl_RGB_Image* aimg, int fittingtype)
{
    if( aimg == NULL )
        return;

    holddraw = true;

    unloadimage();

    imgsrc = aimg;

    holddraw = false;

    if ( imgsrc != NULL )
    {
        label( NULL );
    }
}

void Fl_RenderViewer::unloadimage()
{
    fl_imgtk::discard_user_rgb_image( imgsrc );
}

unsigned long long Fl_RenderViewer::gettimenow()
{
    timeval tv;
    gettimeofday(&tv, NULL);
    return (unsigned long long)(tv.tv_sec * 1000 + tv.tv_usec / 1000);
}

int Fl_RenderViewer::imgw()
{
    if ( imgsrc != NULL )
    {
        return imgsrc->w();
    }

    return 0;
}

int Fl_RenderViewer::imgh()
{
    if ( imgsrc != NULL )
    {
        return imgsrc->h();
    }

    return 0;
}

int Fl_RenderViewer::handle( int event )
{
    int ret = Fl_Box::handle( event );

    switch( event )
    {
        case FL_DND_ENTER:
        case FL_DND_DRAG:
        case FL_DND_RELEASE:
        case FL_DND_LEAVE:
            if ( _notifier != NULL )
            {
                ret = 1;
            }
            else
            {
                ret = 0;
            }
            break;

        case FL_PASTE:
            if ( _notifier != NULL )
            {
                //static const char* retC = Fl::event_text();
                char* retC = strdup( Fl::event_text() );
                if ( retC != NULL )
                {
                    _notifier->OnDropFiles( this, retC );
                    free ( retC );
                }
                ret = 1;
            }
            else
            {
                ret = 0;
            }
            break;

        case FL_PUSH:
            take_focus();
            shift_key = Fl::event_shift();
            mouse_btn = Fl::event_button();
            
            if ( drawruler == true )
                redraw();
            break;

        case FL_MOVE:
            check_x = Fl::event_x();
            check_y = Fl::event_y();

            if ( _notifier != NULL )
            {
                _notifier->OnMouseMove( this,
                                        Fl::event_x() - x(),
                                        Fl::event_y() - y(),
                                        true );
            }
            
            if ( drawruler == true )
                redraw();
            break;

        case FL_LEAVE:
            if ( _notifier != NULL )
            {
                _notifier->OnMouseMove( this, 0, 0, false );
            }
            
            if ( drawruler == true )
                redraw();
            break;

        case FL_RELEASE:
            shift_key = Fl::event_shift();
            mouse_btn = Fl::event_button();

            if ( imgsrc != NULL )
            {
                check_x = Fl::event_x() - x();
                check_y = Fl::event_y() - y();

                if ( check_x < 0 )
                {
                    check_x = 0;
                }

                if ( check_y < 0 )
                {
                    check_y = 0;
                }

                if ( _notifier != NULL )
                {
                    _notifier->OnMouseClick( this,
                                             check_x,
                                             check_y,
                                             (unsigned)mouse_btn );
                }
            }
            //return 1;
            break;

        case FL_MOUSEWHEEL:
            if ( _notifier != NULL )
            {
                _notifier->OnKeyPressed( this,
                                         FL_MOUSEWHEEL,
                                         Fl::event_dx(),
                                         Fl::event_dy(),
                                         0 );
            }
            break;

        //case FL_KEYUP:
        case FL_KEYDOWN:
            {
                bool bredraw = false;

                int kbda = Fl::event_alt();
                int kbdc = Fl::event_ctrl();
                int kbds = Fl::event_shift();
                int kbdk = Fl::event_key();

#ifdef __APPLE__
                if ( kbdc == 0 )
                {
                    kbdc = Fl::event_command();
                }
#endif /// of __APPLE__
                if ( _notifier != NULL )
                {
                    unsigned short kb = kbdk & 0xFFFF;

                    _notifier->OnKeyPressed( this, kb, kbds, kbdc, kbda );
                }

                ret = 1;
            }
            break;
    }

    return ret;
}

void Fl_RenderViewer::draw()
{
    if ( holddraw == true )
        return;

    fl_push_clip( x(), y(), w(), h() );

    Fl_Color prevC = fl_color();

    // Clear Background.
    fl_color( color() );
    fl_rectf( x(), y(), w(), h() );

    int put_x = x();
    int put_y = y();

    if ( drawruler == true )
    {
        if ( ( w() > rulerV ) && ( h() > rulerH ) )
        {
            // draw backs
            fl_color( rulerCbg );
            fl_rectf( x(), y(), w(), rulerV );
            fl_rectf( x(), y(), rulerH, h() );

            // draw fore
            fl_color( rulerCfg );
            fl_rect( x(), y(), w(), rulerV );
            fl_rect( x(), y(), rulerH, h() );

            // vertical ruler
            for( int cntx=(x()+rulerV); cntx<(x()+w()); cntx+=2 )
            {
                int vlen = rulerV / 2;
                
                if ( ( cntx > 0 ) && ( cntx%10 == 0 ) )
                    vlen = rulerV * 0.75f;
                
                fl_line( cntx, y(), cntx, y() + vlen );
            }

            // horizontal ruler
            for( int cnty=(y()+rulerH); cnty<(y()+h()); cnty+=2 )
            {
                int vlen = rulerH / 2;
                
                if ( ( cnty > 0 ) && ( cnty%10 == 0 ) )
                    vlen = rulerH * 0.75f;
                
                fl_line( x(), cnty, x() + vlen, cnty );
            }

            put_x += rulerV;
            put_y += rulerH;
        }
    }
    
    if ( imgsrc != NULL )
    {
        imgsrc->draw( put_x, put_y );
    }

    if ( drawruler == true )
    {
        int mx = Fl::event_x() - x();
        int my = Fl::event_y() - y();
        
        if ( ( mx + x() > x() ) && ( my + y() > y() ) )
        {
        
            fl_color( 0xFF555500 );
                  
            if ( ( mx > rulerV ) && ( my < rulerH ) )
            {
                fl_line( mx, y(), mx, y() + h() );
                
                snprintf( tltip, 100, "X = %d", mx );
            }
            else
            if ( ( mx < rulerV ) && ( my > rulerH ) )
            {
                fl_line( x(), y() + my, x() + w(), y() + my );

                snprintf( tltip, 100, "Y = %d", my );
            }
            else
            {
                snprintf( tltip, 100, "X:Y = %d:%d", mx, my );
            }

            int box_w = 0;
            int box_h = 0;

            fl_font( FL_COURIER, 12 );
            fl_measure( tltip, box_w, box_h, 0 );

            if ( ( box_w > 0 ) && ( box_h > 0 ) )
            {
                // make some margin -
                box_w += 5;
                box_h += 5;
                int box_x = x() + mx - box_w;
                int box_y = y() + my + ( box_h / 2 );
               
                Fl_Color bcol = color();
                Fl_Color tcol = 0xFF555500;
                
                if ( ( box_x < ( x() + rulerV ) ) || ( box_x < 0 ) )
                {
                    box_x = x() + rulerV + 10;
                    box_y -= box_h * 2;
                }
                
                if ( box_y < ( y() + rulerH ) )
                {
                    box_y = y() + rulerH;
                }
                else
                if ( box_y + box_h > y() + h() )
                {
                    box_y -= box_h * 2;
                }

                fl_draw_box( FL_THIN_UP_BOX, box_x, box_y, box_w, box_h, bcol );
                fl_color( tcol );
                fl_draw( tltip, box_x + 2, box_y + box_h - 5 );
            }
        }
    }

    // Draw itself rectangle.
    Fl_Box::draw();
    fl_color( prevC );
    fl_pop_clip();

    if ( _notifier != NULL )
    {
        _notifier->OnDrawCompleted();
    }

}

void Fl_RenderViewer::resize(int x,int y,int w,int h)
{
    Fl_Box::resize( x, y, w, h );

    redraw();
}

void Fl_RenderViewer::box(Fl_Boxtype new_box)
{
    // It always ignore user custom box type.
    //Fl_Scroll::box( box() );
    //Fl_Box::box( FL_FLAT_BOX );
}

void Fl_RenderViewer::type(uchar t)
{
    // It always ignore user type = NO scroll bars.
    // Fl_Scroll::type( type() );
    Fl_Box::type( 0 );
}

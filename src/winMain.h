#ifndef __WINMAIN_H__
#define __WINMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Multiline_Input.H>

#include "Fl_RenderViewer.h"
#include "FLFTRender.h"
#include "cfgldr.h"

#include <pthread.h>
#include <vector>
#include <string>

class WMain : public Fl_RenderViewerNotifier
{
    public:
        WMain( int argc, char** argv );
        ~WMain();

    public:
        int Run();

    public:
        void WidgetCall( Fl_Widget* w );
        static void WidgetCB( Fl_Widget* w, void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                pwm->WidgetCall( w );
            }
        }

    private:
        void createComponents();
        void getEnvironments();
        void setdefaultwintitle();
        void loadFont();
        bool save2png( Fl_RGB_Image* img, const char* fname );
        
    private:
        int     _argc;
        char**  _argv;

    protected: /// inherit to Fl_ImageViewerNotifier
        void OnMouseMove( void* w, int x, int y, bool inside );
        void OnMouseClick( void* w, int x, int y, unsigned btn );
        void OnKeyPressed( void* w, unsigned short k, int s, int c, int a );
        void OnDropFiles( void* w, const char* files );
        void OnDrawCompleted();

    protected:
        Fl_Double_Window*   window;
        Fl_Group*           grpDiv0;
        Fl_Group*           grpDiv01;
        Fl_Group*           grpDiv02;
        Fl_Group*           grpDiv1;
        Fl_Group*           grpDiv11;
        Fl_Group*           grpDiv12;
        Fl_Group*           grpDiv2;
        Fl_Box*             boxDiv0outline;
        Fl_Button*          btnLoadFont;
        Fl_Button*          btnBold;
        Fl_Button*          btnItalic;
        Fl_Value_Input*     inpFontSize;
        Fl_Box*             boxFontName;
        Fl_Multiline_Input* inpText;
        Fl_Button*          btnRender;
        Fl_RenderViewer*    rndViewer;

    private:
        #define             MAX_WINTITLE_LEN        512
        char wintitlestr[MAX_WINTITLE_LEN];

    protected:
        FLFTRender*         frender;
        unsigned char*      fontbuff;
        Fl_RGB_Image*       imgRender;

    protected:
#ifdef SUPPORT_WCHAR
        std::wstring        pathHome;
        std::wstring        pathSystemBase;
        std::wstring        pathUserData;
        std::wstring        pathUserRoaming;
#else
        std::string         pathHome;
        std::string         pathSystemBase;
        std::string         pathUserData;
        std::string         pathUserRoaming;
#endif

    protected:
        std::wstring        pathFont;
        std::string         fontName;
        ConfigLoader*       cfgldr;
};

#endif // __WINMAIN_H__

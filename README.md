# Font Tester

* A simple tool for testing render TTF/TTC/OTF true type fonts on RGBA buffer.

## Testing image
* ![version 0.1.7.36](https://bitbucket-assetroot.s3.amazonaws.com/repository/6zyRxry/2357291594-image.png?AWSAccessKeyId=AKIA6KOSE3BNJRRFUUX6&Expires=1587630804&Signature=h%2F30UCqh3pb1tnD5bBmOSRLC%2FTs%3D)

## Requirements

* [FLTK-1.3.5-2-ts](https://github.com/rageworx/fltk-1.3.5-2-ts)
* [fl_imgtk](https://github.com/rageworx/fl_imgtk)
* [FLFTRender](https://github.com/rageworx/FLFTRender)
* [freetype2](https://freetype.org/download.html)
* [minIni](https://github.com/compuphase/minIni)

# Makefile for Font Tester for MinGW-W64 + M-SYS
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
CC_PREFIX = $(PREFIX)
CC_PATH =

GCC = $(CC_PATH)$(CC_PREFIX)gcc
GPP = $(CC_PATH)$(CC_PREFIX)g++
AR  = ar
FCG = fltk-config --use-images

#Freetype2 configs
FT2_CFLAGS := $(shell pkg-config freetype2 --cflags)
FT2_LFLAGS := $(shell pkg-config freetype2 --libs)

# FLTK configs 
FLTKCFG_CXX := $(shell ${FCG} --cxxflags)
FLTKCFG_LFG := $(shell ${FCG} --ldflags)

# Base PATH
BASE_PATH = .
FIMG_PATH = ../fl_imgtk/lib
MINI_PATH = ../minIni/dev
SRC_PATH  = $(BASE_PATH)/src
LOCI_PATH = /usr/local/include
LOCL_PATH = /usr/local/lib

# TARGET settings
TARGET_PKG = fonttester
TARGET_DIR = ./bin
TARGET_OBJ = ./obj

# DEFINITIONS
DEFS += -D_POSIX_THREADS -DSUPPORT_DRAGDROP
DEFS += -DINI_ANSIONLY
DEFS += -DUNICODE -D_UNICODE
DEFS += -DUSE_OMP -DUSE_SYSPNG

# Compiler optiops 
COPTS += -ffast-math -fexceptions -fopenmp -O3 -s
#COPTS += -g -fopenmp -ffast-math -DDEBUG -DDEBUG_TTF_REGION

# CC FLAGS
CFLAGS += -I$(SRC_PATH)
CFLAGS += -I$(FIMG_PATH) 
CFLAGS += -I$(LOCI_PATH) -Ires
CFLAGS += -I$(MINI_PATH)
CFLAGS += $(FT2_CFLAGS)
CFLAGS += $(FLTKCFG_CXX)
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# Windows Resource Flags
WFLGAS  = $(CFLAGS)

# LINK FLAG
LFLAGS += -L$(FIMG_PATH)
LFLAGS += -L$(LOCL_PATH)
LFLAGS += $(FT2_LFLAGS)
LFLAGS += -lfl_imgtk
LFLAGS += -lbz2
LFLAGS += $(FLTKCFG_LFG)
LFLAGS += -lpthread

# Sources
SRCS = $(wildcard $(SRC_PATH)/*.cpp)
MSRC = $(MINI_PATH)/minIni.c

# Windows resource
WRES = res/resource.rc

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)
MOBJ = $(TARGET_OBJ)/minIni.o
WROBJ = $(TARGET_OBJ)/resource.o

.PHONY: prepare clean

all: prepare continue

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(MOBJ): $(MSRC)
	@echo "Building minIni ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS) $(MOBJ)
	@echo "Generating $@ ..."
	@$(GPP) $(OBJS) $(MOBJ) $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."
